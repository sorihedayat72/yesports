import React from 'react';
import classes from './index.module.scss';

/**
 * A Plaid-inspired custom input component
 *
 * @param {string} value - the value of the controlled input
 * @param {string} type - the type of input we'll deal with
 * @param {string} label - the label used to designate info on how to fill out the input
 * @param {function} onChange - function called when the input value changes
 * @param {function} onFocus - function called when the input is focused
 */
const Input = ({
                       value,
                       type,
                       onChange,
                       onKeyUp,
                       inputRef,
                       autoFocus,
                       placeholder,
                       label,
                       helperText,
                       invalidText,
                       absoluteInvalidText,
                       hasError,
                       inputBoxClass = '',
                       labelClass,
                       ...props
                   }) => {

    const handleOnChange = val => {
        onChange(val);
    };

    return (
        <>
            {(label ||
                helperText ||
                invalidText) &&
            <div className="d-flex justify-content-between" >
                {label && <label className={labelClass}>{label}</label>}
                {/*{helperText &&*/}
                {/*<Tooltip title={<>{helperText}</>} placement="top" className={`${classes.textFieldTooltip} ms-1`}>*/}
                {/*    <InformationC fill="#999"/>*/}
                {/*</Tooltip>*/}
                {/*}*/}
            </div>
            }
            <div
                onKeyDown={props.onKeyDown}
                className={`${classes.InputContainer}
                     ${hasError && classes.InputContainerError} ${props.className}`}>
                <div className={`${inputBoxClass} d-flex justify-content-between align-items-center`}>
                    <input autoFocus={autoFocus}
                           value={value}
                           style={{width: props.width, height: props.height, paddingLeft: props.icon ? 32 : {}}}
                           type={type}
                           onClick={props.onClick}
                           placeholder={placeholder}
                           onChange={e => onChange && handleOnChange(e)}
                           onKeyUp={e => onKeyUp && onKeyUp(e)}
                           ref={inputRef}
                           {...props}
                    />
                    <div className={classes.inputSvg}>
                        {props.icon}
                    </div>
                </div>
                {invalidText &&
                <div className={classes.invalidText}>
                    {invalidText}
                </div>
                }
                {absoluteInvalidText &&
                <div className={classes.absoluteInvalidText}>
                    {absoluteInvalidText}
                </div>
                }
            </div>
        </>
    );
};

export default Input;
