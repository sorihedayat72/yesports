import React from 'react';
import classes from './index.module.scss';

function Button(props) {

    const getButtonClass = () => {
        switch (props.type) {
            case 'disabled':
                return classes.disabledBtn;
            case 'selectedChip':
                return classes.selectedChipBtn;
            case 'chip':
                return classes.chipBtn;
            case 'secondary':
                return classes.secondaryBtn;
            case 'primary':
                return classes.primaryBtn;
            case 'active':
                return classes.activeBtn;
            case 'default':
                return classes.defaultBtn;
            default:
                return '';
        }
    };

    return (
        <button id={props.id}
                style={{width: props.width, height: props.height}}
                className={`${getButtonClass()} ${props.className}`}
                onClick={props.onClick}
                tabIndex={props.tabIndex}
                onKeyUp={props.onKeyUp}
                onKeyDown={props.onKeyDown}
                disabled={props.disabled}>
            {props.children}
        </button>
    );
}
export default Button