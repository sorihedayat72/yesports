import React from 'react';
import Menu from '../menu';
import Footer from '../footer';

const Layout = props => {
    const {Component, pageProps} = props;

    return (
            <div key={Math.random()}>
                <div>
                   <Menu/>
                    <Component {...pageProps} />
                </div>
                <Footer/>
            </div>
    );
};

export default Layout;
