import React from 'react';
import Image from 'next/image';
import classes from './index.module.scss';
import {contentfulLoader} from '../../utility/images-helper';

const PageLoader = props => {
    return (
        <div
            id="pageLoader"
            className={classes.wrapper}
        >
            <Image
                src="/assets/yesports-logo.png"
                loader={contentfulLoader}
                alt="yesports logo"
                width={300}
                height={300}
            />
        </div>
    );
};

export default PageLoader;
