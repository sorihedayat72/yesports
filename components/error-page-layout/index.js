import classes from './index.module.scss';
import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Button from '../../base-components/button';
import Image from 'next/image';
import {contentfulLoader} from '../../utility/images-helper';

export const ErrorPagesLayout = ({
                                     title,
                                     buttonText,
                                     buttonLink,
                                     image,
                                     number,
                                     content,
                                 }) => {
    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            <div className="container">
                <div className="row">
                    <div className={`col-12 ${classes.errorPage} p-3`}>
                        <Image
                            className={classes.image}
                            src={image}
                            alt={`Error ${number}`}
                            loader={contentfulLoader}
                            width={450}
                            height={400}
                        />
                        <h1 className={classes.title}>{title}</h1>
                        <p className={classes.description}>{content}</p>
                        {buttonText && (
                            <Button type="primary" className={classes.errorBtn}>
                                <Link href={buttonLink} passHref>
                                    <span className={classes.linkText}>{buttonText}</span>
                                </Link>
                            </Button>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};
