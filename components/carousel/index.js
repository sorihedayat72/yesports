import Slider from 'react-slick';
import React from "react";
import ShortArrow from "../../icons/short-arrow";

const Carousel = props => {
    const settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll:2,
        prevArrow: <ShortArrow />,
        nextArrow: <ShortArrow />,
    };
    return(
        <div className={props.className}>
            <Slider {...settings}>
                {props.children}
            </Slider>
        </div>
    )
}
export default Carousel