import React from "react";
import classes from "./index.module.scss";
import Button from "../../base-components/button";
import ArrowRight from "../../icons/arrow-right";
import Input from "../../base-components/input";
import Search from "../../icons/search";
import Image from "next/image";
import {contentfulLoader} from "../../utility/images-helper";

const Menu = () => {
    const mainMenu = ['Marketplace','Packs','Metaverse','Yesp Token'];
    return(
        <div className={`${classes.bg} d-flex justify-content-between align-items-center row`}>
            <div className="d-flex col-4 align-items-center">
                    <Image
                        src="/assets/yesports-logo.png"
                        loader={contentfulLoader}
                        alt="yesports logo"
                        width={127}
                        height={53}
                    />
                    <div className={classes.inputMenu}>
                        <Input icon={<Search/>} placeholder="Search" />
                    </div>
            </div>
            <div className="d-flex justify-content-between align-items-center col-8">
                <div className="d-flex justify-content-around w-100 align-items-center">
                    {mainMenu.map((items, index) => {
                        return(
                            <a className={classes.linkMenu} href="#" key={index}>{items}</a>
                        )
                    })}
                </div>
                <Button type='default' className='position-relative'>
                    <div className={classes.insideBtn}>
                        <span>Connect</span>
                        <ArrowRight className={classes.btnSvg}/>
                    </div>
                </Button>

            </div>
        </div>
    )
}
export default Menu