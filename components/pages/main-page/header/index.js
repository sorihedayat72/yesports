import React from "react";
import classes from "./index.module.scss";
import Button from "../../../../base-components/button";
import ArrowRight from "../../../../icons/arrow-right";
import Image from "next/image";
import {contentfulLoader} from "../../../../utility/images-helper";
import Carousel from "../../../carousel";

const Header = () => {
    return (
        <div className={`${classes.header} row`}>
            <div className='col-12 col-lg-4'>
                <h1 className={classes.h1}>
                    <span>Introducing</span>
                    <span>YESPORTS</span>
                    </h1>
                <h2 className={classes.h2}>
                    Get Closer to your
                    Favourite esports teams.
                </h2>
                <h3 className={classes.h3}>
                    All YESPORTS NFTS grant access to the Metaverse.
                </h3>
                <Button type='primary' className='position-relative'>
                    <div className={classes.insideBtn}>
                        <span> View Pack</span>
                        <ArrowRight className={classes.btnSvg}/>
                    </div>
                </Button>
            </div>
            <div className='position-relative col-12 col-lg-6'>
                <div className='d-flex justify-content-around'>
                    <Image
                        src="/assets/avatar-1.png"
                        loader={contentfulLoader}
                        alt="avatar"
                        width={410}
                        height={587}
                    />
                </div>
                <div className={classes.carousel}>
                    <Carousel >
                        <Image
                            src="/assets/avatar-2.png"
                            loader={contentfulLoader}
                            alt="avatar"
                            width={157}
                            height={215}
                        />
                        <Image
                            src="/assets/avatar-3.png"
                            loader={contentfulLoader}
                            alt="avatar"
                            width={157}
                            height={215}
                        />
                        <Image
                            src="/assets/avatar-4.png"
                            loader={contentfulLoader}
                            alt="avatar"
                            width={157}
                            height={215}
                        />
                        <Image
                            src="/assets/avatar-2.png"
                            loader={contentfulLoader}
                            alt="avatar"
                            width={157}
                            height={215}
                            objectFit="cover"
                        />
                        <Image
                            src="/assets/avatar-3.png"
                            loader={contentfulLoader}
                            alt="avatar"
                            width={157}
                            height={215}
                            objectFit="cover"
                        />
                        <Image
                            src="/assets/avatar-4.png"
                            loader={contentfulLoader}
                            alt="avatar"
                            width={157}
                            height={215}
                        />
                    </Carousel>
                </div>
            </div>
        </div>
    )
};
export default Header