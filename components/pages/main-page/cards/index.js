import React from "react";
import Card from "../../../card";
import classes from "./index.module.scss"
import Image from "next/image";
import {contentfulLoader} from "../../../../utility/images-helper";
import Button from "../../../../base-components/button";

const Cards = props => {
    return (
        <div className={`${classes.cards}`}>
            <h2 className={classes.h2}>Rare Cards for sale</h2>
            <span className={classes.span}>See something you like? Make an offer.</span>
                <div className='row align-items-center'>
                    <div className='d-flex col-12 col-lg-7 row' >
                        <div className={`${classes.mbCard} col-xl-4 col-lg-12`}>
                            <Card
                                image={<Image
                                    src="/assets/card1.png"
                                    loader={contentfulLoader}
                                    alt="card"
                                    layout='fill'
                                      objectFit='contain'
                                />}
                                name='LOUD Rare #6'
                                instaProfile={<img
                                    src="/assets/insta-profile.png"
                                    alt="card"
                                />}
                                id='@Team Empire'
                                label='Current Bid'
                                answer='0.01 Matic'
                                cardHeight={classes.smallCard}
                            />
                        </div>
                        <div className={`${classes.mbCard} col-xl-4 col-lg-12`}>
                            <Card
                                image={<Image
                                    src="/assets/card2.png"
                                    loader={contentfulLoader}
                                    alt="card"
                                    layout='fill'
                                      objectFit='contain'
                                />}
                                name='LOUD Rare #6'
                                instaProfile={<img
                                    src="/assets/insta-profile.png"
                                    alt="card"
                                />}
                                id='@Team Empire'
                                label='Current Bid'
                                answer='0.01 Matic'
                                cardHeight={classes.smallCard}
                            />
                        </div>
                        <div className={`${classes.mbCard} col-xl-4 col-lg-12`}>
                            <Card
                                image={<Image
                                    src="/assets/card3.png"
                                    loader={contentfulLoader}
                                    alt="card"
                                    layout='fill'
                                      objectFit='contain'
                                />}
                                name='LOUD Rare #6'
                                instaProfile={<img
                                    src="/assets/insta-profile.png"
                                    alt="card"
                                />}
                                id='@Team Empire'
                                label='Current Bid'
                                answer='0.01 Matic'
                                cardHeight={classes.smallCard}
                            />
                        </div>
                    </div>
                    <div className='col-1'/>
                    <div className='col-12 col-lg-4 d-flex justify-content-end'>
                        <div className={`${classes.mbCard} col-12`}>
                            <Card
                                image={<Image
                                    src="/assets/card4.png"
                                    loader={contentfulLoader}
                                    alt="card"
                                    layout='fill'
                                    objectFit='contain'
                                />}
                                name='LOUD Rare #6'
                                instaProfile={<img
                                    src="/assets/insta-profile.png"
                                    alt="card"
                                />}
                                id='@Boom'
                                label='Current Bid'
                                answer='0.01 Matic'
                                cardHeight={classes.bigCard}
                            />
                        </div>
                    </div>
                </div>
            <Button type='default' className={classes.cardsBtn}>
                Browsw all NFTs
            </Button>
        </div>
    )
};
export default Cards;