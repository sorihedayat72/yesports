import React from "react";
import classes from "./index.module.scss";
import Button from "../../base-components/button";

const Card = props => {
    const {image, name, instaProfile, id, label, answer, cardHeight} = props;
    return(
        <div className={`${classes.card} ${cardHeight}`}>
            <div className={classes.imgCard}>
                {image}
                <div className={classes.detail}>
                    <div className='d-flex justify-content-between flex-wrap'>
                        <div className={classes.name}>
                            {name}
                        </div>
                        <div className={classes.id}>
                            {instaProfile}
                            <span className='ms-1 text-nowrap'>{id}</span>
                        </div>
                    </div>
                    <div className='d-flex justify-content-between flex-wrap mt-3'>
                        <div className='position-relative'>
                            <div className={classes.label}>
                                {label}
                            </div>
                            <div className={classes.answer}>
                                {answer}
                            </div>
                            <div className={classes.line}/>
                        </div>
                        <div>
                            <div className={classes.label}>
                                {label}
                            </div>
                            <div className={classes.answer}>
                                {answer}
                            </div>
                        </div>
                        <Button type='primary' className={classes.nftBtn}>
                            View NFT
                        </Button>
                    </div>
                </div>
            </div>


        </div>
    )
};
export default Card