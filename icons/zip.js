import React from 'react';

const Zip = (props) => {
    return (
        <svg onClick={props.onClick} className={props.className} style={props.style}
             width={props.width ? props.width : '10'} height={props.height ? props.height : '9'}
             viewBox="0 0 10 9" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M7.00337 2.62799L5.25333 3.541L2.6335 4.93106L0.886948 4.01805V2.19517L2.6335 1.28216L4.38005 2.19517V3.68001L5.25333 3.22508V1.73709L2.6335 0.369141L0.0136719 1.73709V4.47298L2.6335 5.84408L5.52579 4.3087L7.00337 3.541L8.74992 4.45086V6.27689L7.00337 7.18991L5.25333 6.27689V4.7889L4.38005 5.24699V6.73182L7.00337 8.09977L9.6232 6.73182V3.99593L7.00337 2.62799Z" fill="#DC00BE"/>
        </svg>

)
};
export default Zip

