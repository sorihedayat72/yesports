import React from 'react';

const ShortArrow = (props) => {
    return (
        <svg onClick={props.onClick} className={props.className} style={props.style}
             width={props.width ? props.width : '15'} height={props.height ? props.height : '26'}
             viewBox="0 0 15 26" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M14 1L2 13L14 25" stroke="white" strokeWidth="1.5"/>
        </svg>
    )
};
export default ShortArrow

