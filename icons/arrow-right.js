import React from 'react';

const ArrowRight = (props) => {
    return (
        <svg onClick={props.onClick} className={props.className} style={props.style}
             width={props.width ? props.width : '26'} height={props.height ? props.height : '20'}
             viewBox="0 0 26 20" xmlns="http://www.w3.org/2000/svg">
        <g filter="url(#filter0_d_372_1842)">
        <path d="M21.5303 10.5303C21.8232 10.2374 21.8232 9.76256 21.5303 9.46967L16.7574 4.6967C16.4645 4.40381 15.9896 4.40381 15.6967 4.6967C15.4038 4.98959 15.4038 5.46447 15.6967 5.75736L19.9393 10L15.6967 14.2426C15.4038 14.5355 15.4038 15.0104 15.6967 15.3033C15.9896 15.5962 16.4645 15.5962 16.7574 15.3033L21.5303 10.5303ZM4 10.75H21V9.25H4V10.75Z" />
        </g>
    <defs>
        <filter id="filter0_d_372_1842" x="0" y="0.477051" width="25.75" height="19.0459" filterUnits="userSpaceOnUse">
            <feOffset/>
            <feComposite in2="hardAlpha" operator="out"/>
            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_372_1842"/>
            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_372_1842" result="shape"/>
        </filter>
    </defs>
    </svg>


);
};
export default ArrowRight;