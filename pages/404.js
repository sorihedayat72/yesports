import { ErrorPagesLayout } from '../components/error-page-layout';
import React from 'react';

export default function Custom404() {
  return (
    <ErrorPagesLayout
      title="Not Found...."
      number="404"
      // image="/assets/errors/404.svg"
      buttonText="Return to home page"
      buttonLink="/"
      content=""
    />
  );
}
