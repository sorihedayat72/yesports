import '../styles/bootstrap-scss/bootstrap.min.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import '../styles/globals.scss'
import Head from "next/head";
import PageLoader from "../components/page-loader";
import Layout from "../components/layout";

const MyApp = props => {
    return <>
        <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <title>yesports</title>
        </Head>

        <div key={Math.random()}>
            {/*<PageLoader/>*/}
            <Layout {...props} />
        </div>
    </>
};

export default MyApp
