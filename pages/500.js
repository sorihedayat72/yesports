import { ErrorPagesLayout } from '../components/error-page-layout';

export default function Custom500() {
  return (
    <ErrorPagesLayout
      title="Server Error"
      number="500"
      // image="/assets/errors/500.svg"
      content="500 error"
    />
  );
}
