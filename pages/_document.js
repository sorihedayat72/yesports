import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html>
                <Head>
                    <meta content="text/html;charset=utf-8" httpEquiv="Content-Type"/>
                    <meta content="utf-8" httpEquiv="encoding"/>
                    <noscript id="jss-insertion-point"/>
                    <meta name="theme-color" content="#0b75cb"/>
                    <meta httpEquiv="Content-Security-Policy" content="upgrade-insecure-requests"/>
                    <meta name="mobile-web-app-capable" content="yes"/>
                    <link rel="preload" href="../public/fonts/Oxanium-VariableFont_wght.ttf" as="font" type="font/ttf" crossOrigin/>
                    <link
                        rel="preconnect"
                        href="../public/assets/yesports-logo.png"
                        as="image"
                    />
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>
            </Html>
        );
    }
}

export default MyDocument;

